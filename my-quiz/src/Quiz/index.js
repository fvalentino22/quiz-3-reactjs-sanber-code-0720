import React, { useState, useEffect } from "react"
import Axios from "axios"
import "./styles.css"
const DisplayFilm = () => {
    const BaseUrl = "http://backendexample.sanbercloud.com/api/movies"
    const [dataFilm, setDataFilm] = useState([])
    const [newData, setNewData] = useState({})
    const [editData, setEditData] = useState({})

    useEffect(()=> {
    const getData = async() => {
        try { 
            const res = await Axios({
            method: 'GET',
            url: BaseUrl,
            });
            setDataFilm(res.data)
        } catch (error) {
            console.log(error.response); 
            return error.response;
        }
        }
        getData()
    }, [])

    const handleCreate = async() => {
        try { 
            const res = await Axios({
            method: 'POST',
            url: BaseUrl,
            data: newData
            });
        } catch (error) {
            console.log(error.response); 
            return error.response;
        }
    }

    const handleDelete = async(data) => {
        try { 
            const res = await Axios({
            method: 'DELETE',
            url: `${BaseUrl}/${data.id}`,
            data: data
            });
        } catch (error) {
            console.log(error.response); 
            return error.response;
        }
    }

    const handleEdit = (data) => {
        console.log(data)
        setEditData(data)
    }

    const editFunction = async(data) => {
        try { 
            const res = await Axios({
            method: 'PUT',
            url: `${BaseUrl}/${data}`,
            data: editData
            });
        } catch (error) {
            console.log(error.response); 
            return error.response;
        }
    }

    return(
        <div>
            <form>
                <div>
                    <label>Title</label>
                    <input type="text" name="title" value={newData.title} onChange={ e => setNewData({...newData, title: e.target.value})}></input>
                </div>
                <div>
                    <label>Description</label>
                    <input type="textarea" value={newData.description} onChange={ e => setNewData({...newData, description: e.target.value})}></input>
                </div>
                <div>
                    <label>Year</label>
                    <input type="number" value={newData.year} onChange={ e => setNewData({...newData, year: e.target.value})}></input>
                </div>
                <div>
                    <label>Duration</label>
                    <input type="number" value={newData.duration} onChange={ e => setNewData({...newData, duration: e.target.value})}></input>
                </div>
                <div>
                    <label>Genre</label>
                    <input type="text" value={newData.genre} onChange={ e => setNewData({...newData, genre: e.target.value})}></input>
                </div>
                <div>
                    <label>Rating</label>
                    <select type="select" value={newData.rating} onChange={ e => setNewData({...newData, rating: e.target.value})}>
                        <option key="1" value="1">1</option>
                        <option key="2" value="2">2</option>
                        <option key="3" value="3">3</option>
                        <option key="4" value="4">4</option>
                        <option key="5" value="5">5</option>
                        <option key="6" value="6">6</option>
                        <option key="7" value="7">7</option>
                        <option key="8" value="8">8</option>
                        <option key="9" value="9">9</option>
                        <option key="10" value="10">10</option>
                    </select>
                </div>
                <button onClick={handleCreate}>Save</button>
            </form>

            <table>
                <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Year</th>
                    <th>Duration</th>
                    <th>Genre</th>
                    <th>Rating</th>
                    <th>Action</th>
                </tr>
                {dataFilm.map((e)=>(
                <tr>
                    <td>{e.title}</td>
                    <td>{e.description}</td>
                    <td>{e.year}</td>
                    <td>{e.duration}</td>
                    <td>{e.genre}</td>
                    <td>{e.rating}</td>
                    <td>
                        <button onClick={()=>handleEdit(e)}>edit</button>
                        <button onClick={()=>handleDelete(e)}>delete</button>
                    </td>
                </tr>
                ))}
            </table>

            <form>
                <div>
                    <label>Title</label>
                    <input type="text" name="title" value={editData.title} onChange={ e => setEditData({...editData, title: e.target.value})}></input>
                </div>
                <div>
                    <label>Description</label>
                    <input type="textarea" value={editData.description} onChange={ e => setEditData({...editData, description: e.target.value})}></input>
                </div>
                <div>
                    <label>Year</label>
                    <input type="number" value={editData.year} onChange={ e => setEditData({...editData, year: e.target.value})}></input>
                </div>
                <div>
                    <label>Duration</label>
                    <input type="number" value={editData.duration} onChange={ e => setEditData({...editData, duration: e.target.value})}></input>
                </div>
                <div>
                    <label>Genre</label>
                    <input type="text" value={editData.genre} onChange={ e => setEditData({...editData, genre: e.target.value})}></input>
                </div>
                <div>
                    <label>Rating</label>
                    <select type="select" value={editData.rating} onChange={ e => setEditData({...editData, rating: e.target.value})}>
                        <option key="1" value="1">1</option>
                        <option key="2" value="2">2</option>
                        <option key="3" value="3">3</option>
                        <option key="4" value="4">4</option>
                        <option key="5" value="5">5</option>
                        <option key="6" value="6">6</option>
                        <option key="7" value="7">7</option>
                        <option key="8" value="8">8</option>
                        <option key="9" value="9">9</option>
                        <option key="10" value="10">10</option>
                    </select>
                </div>
                <button onClick={()=>editFunction(editData.id)}>Save</button>
            </form>
        </div>
    )
}

export default DisplayFilm