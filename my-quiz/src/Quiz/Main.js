import React, { Component } from 'react';
import '../Soal 2/public/css/style.css'
import About from './About'

class Main extends Component {
    render() {
        return (
        <>
        <div className="html">
        <div>
        <link href="public/css/style.css" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet" />
        <link href="public/css/style.css" rel="stylesheet" type="text/css" />
        <header>
          <div className="topnav">
            <img id="logo" src="../img/logo.png" width="200px" style={{float: 'left'}} />
            <a href="contact.html">Contact</a>
            <a href="about.html">About</a>
            <a href="index.html">Home</a>
          </div>
          {/* </nav> */}
        </header>
        </div>
        
        <div className='container'>
            <section>
                <h1>Featured Posts</h1>
                <div id="article-list">
                <div>
                    <a href=""><h3>Lorem Post 1</h3></a>
                    <p>
                        Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
                    </p>
                </div>
                <hr/>
                <div>
                  <a href=""><h3>Lorem Post 2</h3></a>
                  <p>
                    Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
                  </p>
                </div>
                <hr/>
                <div>
                    <a href=""><h3>Lorem Post 3</h3></a>
                    <p>
                        Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
                    </p>
                </div>
                <hr/>
                <div>
                    <a href=""><h3>Lorem Post 4</h3></a>
                    <p>
                        Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
                    </p>
                </div>
                <hr/>
                <div>
                    <a href=""><h3>Lorem Post 5</h3></a>
                    <p>
                        Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
                    </p>
                </div>
                </div>
        </section>
        </div>
        <footer>
            <h5>copyright &copy; 2020 by Sanbercode</h5>
        </footer>
        </div>
        
        </>
        );
    }
}
    //   <div className="App">
    //     <header className="App-header">
    //       <img src={logo} className="App-logo" alt="logo" />
    //       <p>
    //         Edit <code>src/App.js</code> and save to reload.
    //       </p>
    //       <a
    //         className="App-link"
    //         href="https://reactjs.org"
    //         target="_blank"
    //         rel="noopener noreferrer"
    //       >
    //         Learn React
    //       </a>
    //     </header>
    //   </div>

  
  export default Main;